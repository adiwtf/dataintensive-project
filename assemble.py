def download(debug_ = False, name = "result.csv"):
    import urllib.request
    import progressbar
    pbar = None
    downloaded = 0

    def show_progress(count, block_size, total_size):
        nonlocal pbar, downloaded
        if pbar is None:
            widgets = [
                'Download:', progressbar.Percentage(),
                ' ', progressbar.SimpleProgress(
                    format='(%s)' % progressbar.SimpleProgress.DEFAULT_FORMAT),
                ' ', progressbar.widgets.Bar(),
                ' ', progressbar.Timer(),
                ' ', progressbar.FileTransferSpeed(),
                ' ', progressbar.AdaptiveETA()
            ]
            pbar = progressbar.ProgressBar(widgets=widgets, max_value=total_size)
            downloaded = 0
        downloaded += block_size
        if downloaded < total_size:
            pbar.update(downloaded)
    print("Step 1/5:")
    url = 'ftp://85.20.209.231/anagrafica_impianti_attivi/2019/2019_1_tr.tar.gz'
    aname = './anagrafica.tar.gz'
    urllib.request.urlretrieve(url, aname, show_progress)
    pbar.finish()
    pbar = None
    print("Step 2/5:")
    url = 'ftp://85.20.209.231/prezzo_alle_8/2019/2019_1_tr.tar.gz'
    pname = './prezzo.tar.gz'
    urllib.request.urlretrieve(url, pname, show_progress)
    pbar.finish()

    ### vengono estratti i file, si troveranno poi in './ftproot/osservaprezzi/copied/'
    print("Step 3/5: Unpacking...")
    import shutil

    shutil.unpack_archive(aname, './')

    shutil.unpack_archive(pname, './')

    ### viene generato un unico csv nella cartella principale contenente il join delle due tabelle
    print("Step 4/5: Generating dataset...")
    import os
    import pandas as pd
    import sys
    if sys.version_info[0] < 3:
        from StringIO import StringIO
    else:
        from io import StringIO
    half = True
    root = "./ftproot/osservaprezzi/copied/"
    with open(name, "w+", newline='') as out:
        res = None
        even = True
        for filename in os.listdir(root):
            if not filename.startswith("prezzo_alle_8"):
                continue
            if filename.endswith(".csv") and filename != name:
                if even and half:
                    even = not even
                    continue
                with open(root + filename, 'r') as fin:
                    data = ''.join(fin.read().splitlines(True)[1:])
                    name_part = root + '/anagrafica_impianti_attivi-' + \
                        filename.split("-")[1]
                    if os.path.isfile(name_part):
                        if debug_:
                            print(filename + " OK")
                        p8 = pd.read_csv(StringIO(data), sep=";")
                        with open(name_part, 'r') as fin2:
                            data = ''.join(fin2.read().splitlines(True)[1:])
                            an = pd.read_csv(StringIO(data), sep=";")
                            if res is None:
                                res = pd.merge(
                                    an, p8, left_on="idImpianto", right_index=True)
                            else:
                                res = res.append(
                                    pd.merge(an, p8, left_on="idImpianto", right_index=True), ignore_index=True)
                                if debug_ :
                                    print("Added " + str(len(res)) + " records")
            even = not even
        res["Date"] = res["dtComu"].map(lambda s: s.split(" ", 1)[0])
        res.drop(columns=["idImpianto", "idImpianto_x", "dtComu"]
                 ).to_csv(out, sep=";", index=False)
        print("Step 5/5: Removing temporary files...")
        os.remove(aname)
        os.remove(pname)
        shutil.rmtree("./ftproot")
        print("Finished. Result saved in " + name)

