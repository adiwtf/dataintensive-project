def predict_from_group(trained_models, data_rows, data_columns):
    group_index = trained_models.columns.drop(["model", "mean_squared_error", "relative_error", "score"]).tolist()
    return pd.merge(data_rows, trained_models, on=group_index, how='inner').apply(lambda row: row["model"].predict([row[data_columns]])[0], axis=1)

class GroupModelAdapter:
    model = None
    dc = None
    def __init__(self, macromodel, data_columns):
        self.model = macromodel
        self.dc = data_columns
    def predict(self, data):
        return predict_from_group(self.model, data, self.dc)