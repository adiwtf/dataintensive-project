# Progetto: Carburanti


[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

# Contenuto

| Nome | Tipo | Contenuto |
| ------ | ------ | ------ |
| carburanti.ipynb | Jupyter notebook | Sviluppo dei modelli e documentazione |
| assemble.py | Python | Dipendenza di carburanti.ipynb per caricare i dati |
| app/ | Applicazione flask | Struttura dell'AppWeb e modelli binarizzati |
| requirements.txt | File txt | Requisiti del modello e della applicazione |
| scriptFlaskW.bat | Script batch | Eseguibile per caricare l'app flask su Windows |
| scriptFlaskLM.sh | Script bash | Eseguibile per caricare l'app flask su Linux/Mac |
| scriptFlaskServer.sh | Script bash | Eseguibile per caricare velocemente l'app flask su Docker |

# Dataset

https://www.mise.gov.it/index.php/it/open-data/elenco-dataset/2036944-carburanti-archivio-prezzi

Il dataset riporta per ogni distributore (di benzina nel nostro caso) Italiano il prezzo del carburante alle 8 di ogni mattina. Sono disponibili latitudine, longitudine, dati del distributore e prezzo per ogni tipo di benzina.

# Obiettivo
Lo studio si pone come obiettivo la ricerca di una 'logica' all'interno del dataset sopra descritto ovvero fornire una previsione del prezzo stimato dei carburanti conoscendo solo l'esistenza di un distributore e la sua posizione.

# Modello
La documentazione del modello le scelte fatte e la descrizione del problema sono situate dentro il file ipynb carburanti.

# Setup Flask

1. Aprire un terminale e posizionarsi dentro alla cartella del progetto
2. Creare un nuovo ambiente virtuale con venv
    . python -m venv venv
    . (usere una versione di python superiore a python3.X)
3. Attivare l'ambiente virtuale appena creato, tramite linea di comando
    - venv\Scripts\activate.bat (Windows)
    - source venv/bin/activate (su Mac OS, Linux, Docker)
4. È ora possibile lanciare lo script preconfigurato per far partire l'ambiente di debug di flask
    - scriptFlaskW.bat (Windows)
    - scriptFlaskLM.sh (linux/Mac)
    - scriptFlaskServer.sh (Docker)
5. Oppure configurare e lanciare manualmente Flask
    1. Windows
	. pip install -r requirements.txt
    . SET FLASK_APP=app
    . SET FLASK_DEBUG=true
    . SET FLASK_ENV=development
    . python -m flask run
    2. Linux/Mac
    . pip install -r requirements.txt
    . export FLASK_APP=app
    . export FLASK_DEBUG=true
    . export FLASK_ENV=development
    . python -m flask run
    3. Docker (opzione potenzialmente pericolosa)
    . pip install -r requirements.txt
    . export FLASK_APP=app
    . export FLASK_DEBUG=true
    . export FLASK_ENV=development
    . python -m flask run -h 0.0.0.0 -p 5000

