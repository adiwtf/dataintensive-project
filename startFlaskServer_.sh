#!/bin/sh
#python -m venv venv

#source venv/bin/activate

pip install -r requirements.txt

export FLASK_APP=app
export FLASK_DEBUG=true
export FLASK_ENV=development
python -m flask run -h 0.0.0.0 -p 5000
