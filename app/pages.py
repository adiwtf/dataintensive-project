from os import path

import pandas as pd
import numpy as np
import pickle

from flask import render_template, request, abort, redirect, url_for

from . import app, query

import time;

modelLin = None
modelPol = None
data = None

@app.route("/")
def home():
	return form()

#@app.route("/predict")
#def predict():
#	return render_template("predict.html")

def loadData():
    global data
    if data is None:
        data = pd.read_pickle("app/" + url_for("static", filename="data.bin"))
    return data

def loadModel(n):
    global modelLin
    global modelPol
    if n == 0:
        if modelLin is None:
            modelLin = pd.read_pickle("app/" + url_for("static", filename="ModelLin.bin"))
        return modelLin
    elif n == 1:
        if modelPol is None:
            modelPol = pd.read_pickle("app/"+ url_for("static", filename="ModelPol.bin"))
        return modelPol
    else:
        return pd.read_pickle("app/"+ url_for("static", filename="ModelPol.bin"))
@app.route("/map")
def loadMap():
    return render_template("map.html")

@app.route("/result", methods=['GET'])
def resultOnMap():

    mod = int(request.args["model"])
    distance = int(request.args["distance"])
    lat = float(request.args["lat"])
    lon = float(request.args["lon"])
    isSelf = int(request.args["isSelf"])
    #if (mod == 2):
    #    return bigModel(isSelf, lat, lon, distance, mod = mod)
    distance /= 1000
    data = loadData()
    data = query.whichElemIsInArea(data, lat, lon, distance)
    dataFrameModel = loadModel(mod)
    if isSelf == 0:
        data["prezzo0"] = data.apply(lambda row: dataFrameModel['model'][dataFrameModel["Bandiera"] == row["Bandiera"]][dataFrameModel['isSelf'] == 0].values[0].predict([[row["Latitudine"], row["Longitudine"], int(time.time())]]) if not dataFrameModel[dataFrameModel["Bandiera"] == row["Bandiera"]][dataFrameModel['isSelf'] == 0].empty else np.nan, axis=1)
        minimo = data['prezzo0'].min()[0]
        datamin0 = data[data['prezzo0'] == minimo]
        data = data[data['prezzo0'] != minimo]    
        datamin0.fillna(0, inplace=True)
        datamin0 = datamin0.drop_duplicates(subset=["Indirizzo"],keep='first')
        data.dropna(inplace=True)
        data = data.drop_duplicates(subset=["Indirizzo", "Latitudine", "Longitudine", "isSelf"],keep='first')
        return render_template("predict.html", dati=data, min0=datamin0, isSelf=isSelf)
    elif isSelf == 1:
        data["prezzo1"] = data.apply(lambda row: dataFrameModel['model'][dataFrameModel["Bandiera"] == row["Bandiera"]][dataFrameModel['isSelf'] == 1].values[0].predict([[row["Latitudine"], row["Longitudine"], int(time.time())]]) if not dataFrameModel[dataFrameModel["Bandiera"] == row["Bandiera"]][dataFrameModel['isSelf'] == 1].empty else np.nan, axis=1)
        minimo = data['prezzo1'].min()[0]
        datamin1 = data[data['prezzo1'] == minimo]
        data = data[data['prezzo1'] != minimo]
        datamin1.fillna(0, inplace=True)
        datamin1 = datamin1.drop_duplicates(subset=["Indirizzo"],keep='first')
        data = data.drop_duplicates(subset=["Indirizzo", "Latitudine", "Longitudine", "isSelf"],keep='first')
        return render_template("predict.html", dati=data, min1=datamin1, isSelf=isSelf)
    else:
        data["prezzo0"] = data.apply(lambda row: dataFrameModel['model'][dataFrameModel["Bandiera"] == row["Bandiera"]][dataFrameModel['isSelf'] == 0].values[0].predict([[row["Latitudine"], row["Longitudine"], int(time.time())]]) if not dataFrameModel[dataFrameModel["Bandiera"] == row["Bandiera"]][dataFrameModel['isSelf'] == 0].empty else np.nan, axis=1)
        data["prezzo1"] = data.apply(lambda row: dataFrameModel['model'][dataFrameModel["Bandiera"] == row["Bandiera"]][dataFrameModel['isSelf'] == 1].values[0].predict([[row["Latitudine"], row["Longitudine"], int(time.time())]]) if not dataFrameModel[dataFrameModel["Bandiera"] == row["Bandiera"]][dataFrameModel['isSelf'] == 1].empty else np.nan, axis=1)
        minimo0 = data["prezzo0"].min()[0]
        minimo1 = data["prezzo1"].min()[0]
        datamin0 = data[(data['prezzo0'] <= minimo0)]
        datamin1 = data[(data['prezzo1'] <= minimo1)]    
        datamin0.fillna(0, inplace=True)
        datamin1.fillna(0, inplace=True)
        datamin1 = datamin1.drop_duplicates(subset=["Indirizzo"],keep='first')
        datamin0 = datamin0.drop_duplicates(subset=["Indirizzo"],keep='first')
        data = data[(data['prezzo0'] != minimo0) & (data['prezzo1'] != minimo1)]
    data.dropna(inplace=True)
    data = data.drop_duplicates(subset=["Indirizzo", "Latitudine", "Longitudine", "isSelf"],keep='first')

    return render_template("predict.html", dati=data, min0=datamin0, min1=datamin1, isSelf=isSelf)


@app.route("/form")
def form():
    return render_template("form.html")




