from math import sin, cos, sqrt, atan2, radians

def coordinatesToDistance(latd1, lond1, latd2, lond2):
    # approximate radius of earth in km
    R = 6373.0

    lat1 = radians(latd1)
    lon1 = radians(lond1)
    lat2 = radians(latd2)
    lon2 = radians(lond2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c

    return distance

def whichElemIsInArea(data, mylatd, mylongd, distance):
    d = data[data.apply(lambda row: coordinatesToDistance(row["Latitudine"], row["Longitudine"], mylatd, mylongd) < distance, axis=1)]
    return d